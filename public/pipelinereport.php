<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>>

      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">

        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2 id="Pipeline Report">Pipeline Report</h2>
            <p id="Pipeline Download">Click here to download and view <a
                href="https://duke.box.com/s/iu33jq0miakfjky8rq4zx1cotduh90q1">the
                report</a>.</p>
          </header>

          <p id="center"> Built under the auspices of INSPIRES, the MLP research
            infrastructure uses recent advances in computer science to provide
            high-frequency data on civic space and foreign authoritarian
            influence “events”. Event data is a common resource in social
            science research. An “event” in a political event dataset is a
            structured record of a politically relevant occurrence, such as a
            protest or a change in a country’s laws. In collaboration with our
            INSPIRES Consortium partners and USAID, we have developed codebooks
            that define 19 types of events relevant to civic space and 23 events
            capturing influence by foreign authoritarian countries (referred to
            as “resurgent authoritarian influence” or RAI). The consistent
            structure of event datasets allows researchers to track trends over
            time, expose relationships between events, and build predictive
            models. This report explains the process by which the MLP research
            team produces event codings and utilizes them to produce forecasts.
          </p>

          <a href="#" class="image featured"><img
              src="images/earth-11595_1920.jpg" alt="" /></a>
        </article>
      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>


</body>

</html>
