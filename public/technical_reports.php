<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
	<title>Telephasic by HTML5 UP</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
	<div id="page-wrapper">

		<!-- Header -->
		<div id="header-wrapper">
			<div id="header" class="container">

				<!-- Logo -->
				<h1 id="logo"><a href="index.php">DevLab@Duke</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>>

			</div>
		</div>

		<!-- Main -->
		<div class="wrapper">
			<div class="container" id="main">

				<!-- Content -->
				<article id="content">
					<header class="major">
						<h2>Technical Reports</h2>
						<p>Read the report, What Predicts Changes in Civic Space Evidence from Forecasts in Serbia, <a href="/assets/What%20Predicts%20Changes%20in%20Civic%20Space%20Evidence%20from%20Forecasts%20in%20Serbia-2.pdf">here</a>.</p>
					</header>
					<p id="center"> This report uses the INSPIRES data to investigate the political, social, and economic factors that predict changes in civic space over time in Serbia. We begin by reviewing the accuracy of our forecasts for civic space in Serbia over the last year. We then provide a detailed discussion of the specific variables that are highly predictive of changes in civic space. The goal is two-fold: 1) to give readers insight into the full range of conditions that are predictive of future changes in civic space and 2) to provide a sense of which predictors have the biggest impact on those changes in civic space. </p>

					<header class="major">
						<p>Read the report, Are Legal Changes to Civic Space Associated with Protest? Evidence from High-Frequency Cross-National Data, <a href="/assets/Protest%20Report.pdf">here</a>.</p>
					</header>
					<p id="center"> This report uses the INSPIRES data to investigate the association between protests and the passage of laws bearing on civic space. The relationship between protest and legal changes has important policy and academic implications. Practitioners often face decisions about whether to support protest movements in support of legal openings, as well as whether to support movements protesting against legal closures. Previous research provides competing findings on the relationship between protests and legal restrictions on free assembly and civic space. That work, however, has been hamstrung by poor data on both protests and the timing and characteristics of laws bearing on civic space.

						We use data compiled by the <b>International Center for Not-for-Profit Law (ICNL)</b> on the passage of laws affecting civil society from the period 2012-2020. ICNL maintains a database tracking laws bearing on civic space in many countries around the world. DevLab@Duke expanded this data by deploying a team of research assistants to identify laws in countries that ICNL had not covered. The data distinguishes between restrictive laws and enabling laws and also identifies what sphere of civic space the law affects. We focus our analysis on law bearing on: a) freedom of association; and b) the operation of CSOs. Laws related to freedom of association range from restrictions on the ability of citizens to gather to restrictions on the operation of political parties. Laws impacting the operation of CSOs includes those that affect funding, taxation and the lifecycle of CSOs. We combine the ICNL and INSPIRES machine learning data to address the following questions:

						<br>1. Do high levels of protest precede laws specifically restricting the freedom of association or the operations of CSOs?

						<br>2. Do high levels of protest precede laws that enable freedom of association or the operation of CSOs?

						<br>3. Do protests increase in response to laws specifically restricting the freedom of association or the operation of CSOs?

						<br>4. Do protests increase in response to laws that enable freedom of association or the operation of CSOs?
					</p>

					<a href="#" class="image featured"><img src="images/earth-11595_1920.jpg" alt="" /></a>
				</article>
			</div>
		</div>

		<!-- Footer -->
		<div id="footer-wrapper">

			<div id="copyright" class="container">
				<ul class="menu">
					<li id="lang_en"> <a href="#en"> English </a>
					<li id="lang_es"> <a href="#es"> Spanish </a> <br>
				</ul>
				<ul class="menu">
					<li>&copy; Untitled. All rights reserved.</li>
					<li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/jquery.min.js"></script>
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/jquery.dropotron.min.js"></script>
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/browser.min.js"></script>
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/breakpoints.min.js"></script>
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/util.js"></script>
	<script src="http://web-ml4p-01.oit.duke.edu/srv/web/mlp.trinity.duke.edu/mlp-devlab-main/public/assets/js/main.js"></script>

</body>

</html>
