<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title>Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>>

      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2>Machine Learning Key Concepts</h2>
            <p>Supervised Learning and Cross Validation</p>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Machine Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Machine learning is a subfield of artificial intelligence.
              Machines can be programmed to learn to identify patterns in
              observed data, build models that explain the world, and make
              predictions without any pre-programmed rules.
              <br>
              <br>
              Generally, machine learning models first learn from a training
              dataset, which consists of predictors (or ‘features’) and an
              outcome (or ‘target’) variable we want to predict. All information
              is available at this stage. For instance, if we want to predict
              whether people will default on their loans (target) based on their
              income and education (features), we feed the machine a number of
              known cases – let’s say 200. These cases, or items, refer to
              individuals, their incomes and education, and whether or not they
              defaulted. In this example, the machine uses these known cases to
              learn patterns in the association between income, education and
              default behavior. A test dataset is then used to assess the
              model’s predictive accuracy.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Supervised Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p><b>Supervised Learning</b> is a class of machine learning in
              which an algorithm is designed to map an input onto an output
              using examples. We use supervised learning when the outcome we
              want to predict is organized into discrete categories – like in
              the example above. Our outcome, defaulting on a loan, has clear
              labels in the training dataset (the borrower defaults: yes or no).
              This is, therefore, a classification problem. We could also have a
              continuous variable, such as income, distance, or population. For
              these variables, any value is possible within a given range. These
              outcomes pose a regression problem. Both regression and
              classification problems are part of supervised learning: the
              machine returns an actual prediction for each new case based on
              the outcome in the training dataset.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Cross-Validation</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p><b>Cross-validation</b> is a tool for assessing the accuracy of
              models. In cross-validation, we hold out a portion of the training
              data, which is called the validation dataset. It is used to see
              how well our model works on data that the machine has neither seen
              nor used to train the model. If the model performs well, it should
              perform well in predicting outcomes in the training dataset and in
              the validation dataset. For this project, we use k-fold
              cross-validation. The original dataset is randomly divided into k
              equal sized groups. Each group is then used as a test dataset and
              the rest of the groups as training data. The process is repeated k
              – 1 times. Doing this helps prevent overfitting, which means that
              the model predicts the training set so accurately that it can’t
              produce reliable predictions on new data –the prediction is too
              specific and cannot be generalized.</p>
          </section>


        </div>

        <a href="#" class="image featured"><img
            src="images/markus-spiske-466ENaLuhLY-unsplash.jpg" alt="" /></a>

        <article id="content">
          <header class="major">
            <p>Data Reduction and Neural Networks</p>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Principal Component Analysis (PCA)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p><b>Principal component analysis (PCA)</b> reduces complex data to
              its essential components. Often, large datasets have many features
              that do not contribute to explaining the outcome we are studying.
              PCA creates ‘principal components’, which combine the information
              from the most informative features. It is a fast and simple method
              that improves model efficiency and helps us understand the data
              better without sacrificing important variation. However, PCA
              produces a ‘linear’ representation of the data, meaning that data
              is mapped using linear transformation. With machine learning and
              neural networks, we can capture non-linearity in the data
              structure that PCA may miss.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Neural Networks</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p><b>Neural networks</b> are designed to resemble the operation of
              the human brain. This makes them particularly useful for dealing
              with complex non-linear relationships. Neural networks take one
              input, for example, a picture, and process it through one or more
              layers that can comprehend different aspects of the picture in
              order to produce an output. This output is a probability that what
              is depicted matches a given category. This process of deep
              learning is abstract and not task-specific, unlike the methods
              described above. Each layer has different nodes, which act as
              ‘neurons’: if an object, shape or color is recognized, that node
              is activated and has greater influence on the final prediction.
              Convolutional neural networks (CNNs) and recurrent neural networks
              (RNNs) are the most common neural networks and are used in many
              computer vision tasks, such as classifying images and detecting
              objects, as well as natural language processing (NLP), which is
              how machines understand human languages.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Reinforcement Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p><b>Reinforcement learning</b> is the process of using repeated
              interactions to teach the machine a goal. In reinforcement
              learning, the machine plays a ‘game’ in which it tries to maximize
              the reward. It uses a trial-and-error approach, learning after
              each attempt. Machines that solve Rubik’s Cubes or play chess are
              trained using reinforcement learning.</p>
          </section>


        </div>


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2>Machine Learning Key Terms</h2>
            <p>Some commonly used key terms.</p>
            <a href="#" class="image featured"><img
                src="images/nasa-Q1p7bh3SHj8-unsplash.jpg" alt="" /></a>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Classification Model</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A machine learning model that categorizes values according to two
              or more discrete classes. For example, a natural language
              processing classification model could determine whether a news
              article is reporting on a protest, a change to electoral laws, or
              some other type of event.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Cross - Validation</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A procedure for estimating how well a model performs at making
              classifications or predictions. Cross-validation repeatedly tests
              a model on different subsets of a dataset to measure performance.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Features</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Attributes of an example, either numerical or categorical.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Lasso Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Lasso regression is a type of analysis used in machine learning
              that engages in both variable selection as well as regularization.
              The goal is to enhance the prediction accuracy and
              interpretability of the statistical model produced.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Layer</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A collection of nodes operating together at the same depth within
              a neural network.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Linear Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A regression that uses conditional means to analyze how a dataset
              predicts a target variable.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Logistic Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A regression algorithm that produces a model that generates a
              probability for each discrete label value in classification
              problems using a sigmoid function.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Machine Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A sub-field of artificial intelligence. Machines learn to
              identify patterns in data, build models that explain the world,
              and make predictions from datasets.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Natural Language Processing (NLP)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Systems through which computers learn to process large amounts of
              human spoken language.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Neural Networks</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A model designed to connect multiple nodes in layers to analyze a
              dataset.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Node</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A ‘neuron’ within a neural network. It receives one or more
              weighted inputs and sends an output to the nodes in the next layer
              determined by an activation function. Nodes are organized into
              layers to comprise a network.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Overfitting</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Overfitting occurs when the model predicts the training set so
              accurately that it can’t produce reliable predictions on new data.
              This happens when the model incorporates noise or details that are
              specific to the training set. When accuracy over the training data
              increases and accuracy in the validation data stays the same or
              decreases, we are overfitting.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Principal Component Analysis (PCA)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A statistical procedure that uses an orthogonal linear
              transformation of data which projects n examples with i features
              onto a hyper plane to minimize projection error. This process
              converts a set of observations of potentially correlated variables
              into a set of linearly uncorrelated variables called principal
              components.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Reinforcement Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A subfield of machine learning where the machine perceives its
              environment’s state as a vector of features. The machine can
              execute actions which provide different rewards. The goal of
              reinforcement learning is for the machine to learn a policy, which
              is the optimal action to perform in each state.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Supervised Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The problem of learning a model for which labeled examples are
              available.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Support Vector Machines (SVMs)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Models used for classification that seeks to maximize the
              distance between positive and negative classes.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Target Variable</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The quantity that the model tries to predict.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Test Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Data that is held out from the dataset and is used to assess
              final model validity.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Training Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A collection of examples from the dataset that the algorithm uses
              to create a model.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Unsupervised Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A problem where, given an unlabeled dataset, the algorithm seeks
              to find hidden (or latent) structure.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Validation Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A holdout dataset used to conduct testing for hyperparameter
              tuning with the goal of minimizing overfitting.</p>
          </section>

        </div>
      </div>
    </div>



    <div id="promo-wrapper">
      <section id="promo">
        <h2>Check out our forecasts</h2>
        <a href="forecasts.php" class="button">Forecasts</a>
      </section>
    </div>




  </div>
  </div>

  <!-- Footer -->
  <div id="footer-wrapper">

    <div id="copyright" class="container">
      <ul class="menu">
        <li id="lang_en"> <a href="#en"> English </a>
        <li id="lang_es"> <a href="#es"> Spanish </a> <br>
      </ul>
      <ul class="menu">
        <li>&copy; Untitled. All rights reserved.</li>
        <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
      </ul>
    </div>
  </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>

</body>

</html>
