<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">ML4P</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>>

      </div>

      <!-- Hero -->
      <section id="hero" class="container">
        <header>
          <br>
          <br>
          <h2 id="Technical Details & Research"> Technical Details & Research
          </h2>
        </header>
      </section>

    </div>


    <!-- Main -->
    <h2 id="Technical Details">Technical Details</h2>
    <div class="wrapper">
      <div class="container" id="main">
        <!-- Content -->

        <div class="row features">

          <!-- <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h2><b> <a href= "mlconcepts.php">Machine Learning Key Concepts</a> </b></h2>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p id="KC Description">Machine learning is a subfield of artificial intelligence. Machines can be programmed to learn to identify patterns in observed data, build models that explain the world, and make predictions without any pre-programmed rules. For more information about key concepts underpinning the machine learning approach we use, please see this page.</p>
							</section>

                            <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h2><b> <a href= "keyterms.php" id="Key Terms">Machine Learning Key Terms</a> </b></h2>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p id="KT Description"> Understanding the technical language around machine learning can be complicated. We have provided a glossary of terms to help users understand the basics to utilize this data. See this page to scroll through this glossary.</p>
							</section> -->

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="dimensionsofcs.php"
                    id="Dimensions of CS">Dimensions of Civic Space</a> </b>
              </h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Dimensions of CS Description"> We have identified 19 event
              types that comprise the key components of Civic Space we are
              tracking. We define an event as an action that affects civic space
              openness. An event is extracted from a news article and is coded
              using machine learning to one of the types listed here.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="dimensionsofrai.php"
                    id="Dimensions RAI">Dimensions of Resurgent Authoritarian
                    Influence</a> </b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Dimensions RAI Description"> We have identified 22 event
              types that comprise the key components of Resurgent Authoritarian
              Influence we are tracking. We define an event as an action by an
              authoritarian government to wield influence within a developing
              economy. An event is extracted from a news article and is coded
              using machine learning to one of the types listed here.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="assets/Pipeline_Report_January_2022.pdf"
                    id="Pipeline Report">Pipeline Report</a> </b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Dimensions RAI Description"> The process that underpins our
              research involves numerous steps that ensure data quality and
              forecast robustness. For a detailed explanation of the end-to-end
              processes that drive this project, including data collection and
              processing and the machine learning that drives our forecasts,
              please see this page.</p>
          </section>
        </div>
      </div>
    </div>
    <h2 id="Research">Research (Reports or Research Papers)</h2>
    <div class="wrapper">
      <div class="container" id="main">

        <!-- Content -->

        <div class="row features">

          <!-- <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h3><b> <a href= "/assets/What_Predicts_Changes_in_Civic_Space_Evidence_from_Forecasts_in_Serbia.pdf" id="Changes in Civic Space">What Predicts Changes in Civic Space Evidence from Forecasts in Serbia</a> </b></h3>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p id="Changes in Civic Space Description"> This report uses the INSPIRES data to investigate the political, social, and economic factors that predict changes in civic space over time in Serbia. We begin by reviewing the accuracy of our forecasts for civic space in Serbia over the last year. We then provide a detailed discussion of the specific variables that are highly predictive of changes in civic space. The goal is two-fold: 1) to give readers insight into the full range of conditions that are predictive of future changes in civic space and 2) to provide a sense of which predictors have the biggest impact on those changes in civic space.</p>
							</section> -->

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="/assets/report_7.pdf"
                    id="Legal Changes & Protest">Legal Changes & Protest:
                    Evidence from High-Frequency Data</a> </b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Legal Changes & Protest Description">This report uses the
              INSPIRES data to investigate the association between protests and
              the passage of laws bearing on civic space. The relationship
              between protest and legal changes has important policy and
              academic implications. Practitioners often face decisions about
              whether to support protest movements in support of legal openings,
              as well as whether to support movements protesting against legal
              closures. Previous research provides competing findings on the
              relationship between protests and legal restrictions on free
              assembly and civic space. That work, however, has been hamstrung
              by poor data on both protests and the timing and characteristics
              of laws bearing on civic space.</p>
          </section>
          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="/assets/mlp_intl_vs_local.pdf"
                    id="Reporting on Civic Space">Reporting on Civic Space</a>
                </b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Reporting on Civic Space Description">Getting an accurate
              picture of any country’s civic space is difficult. While many
              analysts rely on the international news, the vast majority of news
              coverage on any given country is the news media in that country.
              The INSPIRES Machine Learning for Peace team has spent enormous
              time ensuring it is extracting as much news as possible from
              national sources. But what are the returns to all that effort?</p>
          </section>
          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="/assets/Tanzania_ML4P.pdf"
                    id="Tanzania_ML4P">Democratic Backsliding and Media
                    Responses to Government Repression</a></b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Tanzania_ML4P Description">A key feature of the global wave
              of democratic backsliding is that aspiring autocrats seek to
              influence the media through legal restrictions. We develop an
              original argument linking media characteristics to the regulatory
              environment and test it using a huge corpus of electronic media in
              Tanzania. We employ two state-of-the-art machine learning models
              to classify the topics and sentiment of news stories and exploit a
              significant legal change that targeted media houses. We find that
              critical news sources censor the tone of their articles but
              continue to cover the same topics; we also find that international
              news sources do not fill the hole left by a critical domestic
              press. The paper sheds light on the conditions under which the
              press can be resilient in the face of legal threats.</p>
          </section>
          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="/assets/cc_manuscript.pdf" id="">The Effect of
                    Government Repression on Civil Society: Evidence from
                    Cambodia</a></b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="">To limit oversight by civil society, governments often
              repress NGOs. However, quantitative research has yet to
              investigate how restricted civic space impacts the behavior of
              NGOs operating in diverse sectors. Surveying employees from 106
              NGOs in Cambodia, we employ a conjoint experiment to identify how
              the prevalence of repression affects NGOs’ pursuit of funding via
              grant applications. We find that although increases in the
              perceived prevalence of harassment has a stronger deterrent effect
              on advocacy work, harassment also deters NGOs focused on service
              delivery. Our results suggest that local officials target both
              advocacy and service delivery NGOs, but for different reasons.</p>
          </section>
          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a
                    href="/assets/The_Effect_of_NGO_Laws_on_Bilateral_Aid_Flows.pdf"
                    id="">The Effect of Closing Civic Space on Aid: Heterogenous
                    Donor Responses to NGO Laws</a></b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="">Government efforts to restrict civic space have increased
              dramatically, especially in aid-receiving countries. How do donors
              respond to these attacks, and do their responses vary according to
              how they prioritize support for advocacy work? We investigate
              these questions using dyadic data on aid flows, original global
              data tracking restrictive NGOs laws, and a variety of research
              designs. We find strong evidence that advocacy-oriented donors
              back down by disproportionately decreasing support for advocacy as
              it becomes more difficult to work with local partners. The
              findings advance our understanding of the costs and benefits
              aid-receiving countries face when engaging in democratic
              backsliding.</p>
          </section>
          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b> <a href="/assets/RAI_INSPIRES_Reports.pdf" id="">Resurgent
                    Authoritarian Influence: New Machine-Generated,
                    High-Frequency, Cross-National Data</a></b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="">This research memo reviews the academic and policy
              literatures on Resurgent Authoritarian Influence (RAI), discusses
              existing data, and describes the MLP RAI data. We group our 22 RAI
              events into 5 conceptual categories and summarize the prevalence
              of reporting on these categories across countries and over time
              using visualizations, descriptive statistics, and dimensionality
              reduction. Thhis analysis suggest that RAI activity has been
              surprisingly consistent over the last ten years, that Russia and
              China utilize regionally-specific approaches to exerting
              influence, and that Russia and China deployed similar strategies
              when dealing with strategically important countries with which
              they have a strained or hostile relationship.</p>
          </section>
        </div>

      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
