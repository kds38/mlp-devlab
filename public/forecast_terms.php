<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title>Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">DevLab@Duke</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>

      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <article id="content">
          <br>
          <header class="major">
            <h2>Forecast Terms</h2>
            <p>Some commonly used key terms.</p>
          </header>

        </article>

        <div class="row features">
          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>10 yr Bond</h3>
            </header>
            <p>The interest rate return on a 10 year government bond.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Consumer Confidence</h3>
            </header>
            <p>A measure of the optimism that consumers feel about the current
              state of the economy as well as their own personal finances.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Consumer Credit</h3>
            </header>
            <p>A measure that attempts to capture the total amount of credit
              available to consumers in a country.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Consumer Price Index</h3>
            </header>
            <p>A measure of the average price of a set basket of goods within a
              country that moves over time.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Current Account</h3>
            </header>
            <p>A measure that consists of the balance of trade, net primary
              income and net transfers, that have taken place over a given
              period of time.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Food Price Inflation</h3>
            </header>
            <p>A measure of the extent to which the prices of food within a
              country have increased or decreased over time.</p>
          </section>


          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Forecast Confidence</h3>
            </header>
            <p>Forecast Confidence captures the extent to which our forecast
              explains the in-sample variance of our historical data. It gives
              us a gauge on how reliable we expect our numbers to be as we make
              predictions.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Gross Domestic Product (GDP)</h3>
            </header>
            <p>A measure that captures the level of output of an economy.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>GDP Growth Rate</h3>
            </header>
            <p>A measure of the growth rate of GDP over time.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>GDP Index</h3>
            </header>
            <p>An index variable that considers several variables related to GDP
              and creates a composite score that measures how they move over
              time.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <header>
              <h3>Money Supply</h3>
            </header>
            <p>A measure of the total amount of currency available within an
              economy at a given time.</p>
          </section>

        </div>


      </div>
    </div>

    <div id="promo-wrapper">
      <section id="promo">
        <h2>For more information about the underlying processes that drive our
          forecasts, please see this report.</h2>
        <a href="pipelinereport.php" class="button">Pipeline Report</a>
      </section>
    </div>
  </div>

  <!-- Footer -->
  <div id="footer-wrapper">

    <div id="copyright" class="container">
      <ul class="menu">
        <li id="lang_en"> <a href="#en"> English </a>
        <li id="lang_es"> <a href="#es"> Spanish </a> <br>
      </ul>
      <ul class="menu">
        <li>&copy; Untitled. All rights reserved.</li>
        <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
      </ul>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>

</body>

</html>
