<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php"></a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>

      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2 id="Process">The Forecasting Process</h2>
            <p> </p>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2 id="Collection"><b>Data Collection</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Collection Description">We collect articles from a variety of
              major online newspapers. We not only scrape international news
              sources, but also include major (by circulation) local newspapers
              within countries.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2 id="Processing"><b>Data Processing</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Processing Description">After we have collected the articles,
              they are run through a neural network that determines whether each
              individual piece of data relates to civic space changes. The
              neural network then codes data to a variety of civic space event
              types.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2 id="Indexing"><b>Data Indexing</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Indexing Description">We then use all of our civic space
              event data and a 'Principal Component Analysis' (PCA) to generate
              a monthly civic space index. Lower scores indicate an increase in
              openness of civic space and a higher score indicates a decrease.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2 id="Building"><b>Building a Forecast</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Building Description">We combine the civic space event data
              with high-frequency economic data and then use regression analysis
              to forecast changes in civic space 1, 3 and 6 months into the
              future. More specifically, we use Lasso Regression in order to
              make the models more parsimonious and identify the key predictors
              of civic space.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2 id="Validation"><b>Forecast Validation</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p id="Validation Description">Finally, we rely on a survey of
              local, country-based experts to ensure that we are accurately
              capturing news and events on the ground. Experts provide
              information on electronic sources they are reading, as well as
              key, recent events bearing on civic space. In return, we provide
              local experts with our forecasts as a tool they can use in their
              own work with civil society.</p>
          </section>

        </div>

        <!-- <div id="promo-wrapper">
					       <section id="promo">
						      <h2>Check out our forecasts</h2>
						      <a href="forecasts.php" class="button">Forecasts</a>
					       </section>
				        </div> -->

      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
