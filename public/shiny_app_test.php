<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title id="title">Telephasic by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
	</head>
	<body class="no-sidebar is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div id="header" class="container">

						<!-- Logo -->
							<h1 id="logo"><a href="index.php">ML for Peace</a></h1>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a></li>
									<li><a href="kibana.php" id="Data_Dashboard_Link">Data Dashboard</a></li>
									<li class="break">
                                    <li><a href="technical_details.php" id="Technical_Details_&_Research_Link">Technical Details</a></li>
									<li><a href="team.php" id="Our_Team Link">Our Team</a></li>
								</ul>
							</nav>

					</div>

					<!-- Hero -->
						<section id="hero" class="container">
							<header>
                                <br>
                                <br>
								<h2 id="Forecasts"> Forecasts</h2>
							</header>
						</section>
				</div>

			<!-- Main -->
				<div class="wrapper">
					<div class="container" id="main">

						<!-- Content -->
							<article id="content">
								<header class = "major" id="FC">
									INSPIRES is testing how machine learning can help predict changes in civic space before they occur in select countries around the world. Machine learning (ML) is a set of methods for training computers to learn from data, where 'learning' includes the detection of patterns or structure in data. After applying ML tools to the data that we collect and process, we produce regular, detailed reports that describe our findings and provide time-sensitive insights that might aid citizens, civil society, and the media in identifying emerging threats to civic space.

More information on our forecasting process is available <a href = "forecastingprocess.php">here</a>. A guide to frequently used terms is available <a href = "forecast_terms.php" >here</a> .
<br>
<br>
<!-- We have also built a web application that allows users to generate their own forecasts for select civic space events in select countries. Using the application below, you can select a country and event of interest, generate a forecast using our data and forecasting models, and see a basic description of the results. To access our detailed forecasting report for a specific country, see the contact information provided below. -->


								</header>
								<iframe src="https://mlp-devlab.shinyapps.io/forecast/" height="1800" width="1200"></iframe>
								<!-- <iframe src="https://mlp-devlab.shinyapps.io/descriptive/" height="1800" width="1200"></iframe> -->
							</article>
					</div>
				</div>

			<!-- Footer -->
			<div id="footer-wrapper">
					
					<div id="copyright" class="container">
					<ul class="menu">
							<li id="lang_en"> <a href="#en"> English </a> <li id="lang_es"> <a href="#es"> Spanish </a> <br>
						</ul>
						<ul class="menu">
							<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>
				</div>
		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<script src="assets/js/getText.js"></script>

	</body>
</html>
