<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title>Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2><b>Dimensions of Civic Space</b></h2>
            <p>The events below comprise the key components of Civic Space we
              are tracking. We define an event as an action that affects civic
              space openness. An event is extracted from a news article and is
              coded using machine learning to one of the 19 types listed here.
              These event codings allow us to track civic space at high
              frequency, which in turn allows us to understand how shifts in
              civic space are happening over time. This in turn gives us the
              ability to forecast how civic space might change in coming months.
              This list has since been updated several times, most recently in
              August 2021.</p>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Arrests</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>An institution within the government-controlled security
              apparatus --i.e. the police, the military, or other-- apprehends
              people or groups of people who are part of an opposition movement
              or party, a civil society organization, a media organization, or a
              protest.
              <br>
              <br>
              <b>Example:</b> "The Police in Abia on Wednesday arrested 51
              persons suspected to be members of the proscribed Indigenous
              People of Biafra, claiming they ‘operate as members of Judaism,’
              in Umuahia."
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Censor</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The government actively prevents free speech by individuals in
              the media, in public or online. This includes banning certain
              content from individual speech or news stories, dictating how
              certain concepts or people can be referred to in public speech, or
              directly dictating agenda setting for media organizations. This
              category also includes the government censoring internet websites,
              internet shutdowns, fines on independent media, limitations on
              foreign ownership of media outlets, and political actors gaining
              influence within media organizations. Magnitude is a scale (see
              below; try to determine the importance of the target with
              information given).
              <br>
              <br>
              <b>Example:</b> “The Tanzanian government has suspended newspaper
              The Citizen for seven days after the publication ran a story on
              the falling value of the Tanzanian shilling.”
            </p>
          </section>

          <!-- <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h3><b>Changes to Elections</b></h3>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p>The executive alters the rules around elections, usually with the aim of benefiting electorally. This includes rescheduling/postponing/cancelling regularly-scheduled elections, calling irregular elections or constitutional referenda, hamper the work of independent election observers, erode the autonomy/authority of the electoral commission, as well as any other institutional change that directly affects the electoral process.
                                <br>
                                <br>
                                <b>Example:</b> “Nigeria’s electoral authority has delayed presidential and national assembly elections by one week amidst protests from the two main opposition parties. The government alleged logistical problems with ballot delivery to justify the delay.”</p>
							</section> -->

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Cooperation</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Domestic political, social, or business actors collaborate on one
              or a range of issues or demonstrate an intent to do so. This does
              not include intragovernmental cooperation, except for local and
              federal cooperation. Cooperation indicates a willingness for
              domestic actors to work together to resolve important issues.
              Cooperation involving international actors is not a civic space
              event, but is an RAI event.
              <br>
              <br>
              <b>Example:</b> “Opposition movement NJPE has pledged to work
              together with President Muhammad to reduce rampant poverty in the
              capital’s outskirts.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Corruption</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The abuse of entrusted power for private gain. This includes
              street-level corruption (for instance, bribes to police),
              high-level corruption (in public contracting, for instance), and
              legal actions around corruptions. </p>
            <br>
            <br>
            <b>Example:</b> “Ex-President Sarkozy gets Jail Sentence for
            Corruption in France”</p>
            <!-- <br>
                                <br>
								Actor: NJPE
								<br>
								Target: President Muhammad -->
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Coup</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Changes in government or persistence of government that are not
              in the rules of succession or transition. Coups, refusing to cede
              power, or a power grab after an unfair election are examples of
              this event type. Peaceful government transitions are nonviolent
              transfers of power or legitimate continuity of government elected
              by democratic means and accepted by a majority of political
              forces. Code peaceful change as ‘1’ in the direction column.
              Speech is the pledge to accept the result of an election (a threat
              of a coup falls under ‘political threat’).
              <br>
              <br>
              <b>Example:</b> “Forces loyal to Turkey’s president quashed a coup
              attempt in a night of explosions, air battles and gunfire that
              left at least 161 people dead and 1,440 wounded, yesterday.
              President Recep Tayyip Erdogan vowed that those responsible “will
              pay a heavy price for their treason”.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Defamation Case</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Cases in which an individual or a group related to an opposition
              movement or party, current or former government officials, a civil
              society organization, a media organization, a business leader, a
              member of a minority group, or a protest are accused, usually by a
              government official, of: directly defaming/libeling/slandering the
              government or one or some of its members. Alternatively, these
              are: strategic lawsuits against civic actors to intimidate public
              participation. This category is a subset of the 'Legal Action'
              event type (see below).
              <br>
              <br>
              <b>Example:</b> “A Phnom Penh court on Friday found veteran
              opposition chief Sam Rainsy guilty of defaming Prime Minister Hun
              Sen and ordered him to pay damages of $1 million, the latest blow
              to an opposition crippled by legal cases this year.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Disaster</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>This category includes: (1) natural disasters such as
              earthquakes, hurricanes, floods, famines or food crises or any
              catastrophic event that results or may result in serious damage.
              loss of life, and/or political implications; (2) infrastructural
              accidents that can endanger the population –a dam or pipe
              bursting, the sinking of a ferry, etc.) The goal is to focus on
              the types of events that might reflect on government
              accountability and civic space, not minor events like traffic
              accidents. <b>Note:</b> this includes deaths or economic strife
              caused by disasters, including reports on deaths/infections caused
              by COVID-19.
              <br>
              <br>
              <b>Example:</b> ROMBO district residents in Kilimanjaro region are
              in danger of facing food shortage if concerted efforts to control
              wildlife in consuming crops are not employed.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Election Activity</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Reporting on regular electoral activities including the
              introduction of candidacies, the conduct of campaigns, the
              announcement of results, and the formal transfer of power for
              public office. This includes situations where the incumbent
              party/candidate retains power. Normal election results are an
              example of this event type. Specifically, this includes peaceful
              government transitions that are nonviolent transfers of power or
              legitimate continuity of government but does not include coups,
              which are accounted for separately.
              <br>
              <br>
              <b>Example:</b> “The President of the Executive Board of the
              Serbian Progressive Party, Darko Glisic, said that the SNS won a
              convincing victory with 60.2 percent of the votes, based on the
              processed 221 out of 231 polling stations where the voting for the
              parliamentary elections in Serbia was repeated.”
            </p>
          </section>
          <!--
                            <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h3><b>Leadership Changes</b></h3>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p>Changes in government that are the result of a standard election process called in accordance to the rules laid out in the constitution. This includes situations where the incumbent party retains power. Normal election results are an example of this event type. Specifically, this includes peaceful government transitions that are nonviolent transfers of power or legitimate continuity of government but does not include coups, which are accounted for separately.
                                <br>
                                <br>
                                <b>Example:</b> “The President of the Executive Board of the Serbian Progressive Party, Darko Glisic, said that the SNS won a convincing victory with 60.2 percent of the votes, based on the processed 221 out of 231 polling stations where the voting for the parliamentary elections in Serbia was repeated.”
								<br>
								Target: Kosovo President </p>
							</section> -->

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Election Irregularities (e.g., voter suprression or
                  modernizing technology)</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The altering or attempted altering through legal or extra-legal
              manipulation of the conduct, rules or results of elections or
              electoral processes. This could include
              rescheduling/postponing/cancelling regularly-scheduled elections,
              calling irregular elections or constitutional referenda, hampering
              the work of independent election observers, eroding the
              autonomy/authority of the electoral commission, as well as any
              other institutional change that directly affects the electoral
              process. This additionally could include the cancellation of party
              lists and the boycott of elections by opposition, as well as calls
              for recounts or other forms of contesting elections. Finally, this
              includes vote-buying, intimidation, or vote-rigging. This does not
              include standard election proceedings.
              <br>
              <br>
              <b>Example:</b> “Nigeria’s electoral authority has delayed
              presidential and national assembly elections by one week amidst
              protests from the two main opposition parties. The government
              alleged logistical problems with ballot delivery to justify the
              delay.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Legal Action</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Legal action refers to the prosecution or investigation of
              criminal activity or disputes over government authority, as well
              as the trials, convictions or dismissals that result. This event
              is related to civic space if the targeted people are part of an
              opposition movement or party, current and former government
              officials, a civil society organization, an NGO, a media
              organization or member, a (legal or illegal) business leader,
              member of minority group, or a protest. This specifically does not
              include arrests, which are defined as its own event type.
              Defamation cases are a subset of ‘Legal Action’ (see above).
              <br>
              <br>
              <b>Example:</b> “Kosovo President is Indicted for War Crimes for
              Role in War with Serbia.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Legal Change</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Legal change refers to any changes or proposed changes in the
              laws of a nation in such a way as to affect civic space. This
              includes legal restrictions on speech, political groups, NGOs, and
              the changing of constitutions as well as changing of the powers of
              the government. This also includes some restrictions on assembly,
              but does not include curfews and other martial law declarations,
              which are specifically covered by other event categories.
              <br>
              <br>
              <b>Example:</b> “Venezuela’s constituent assembly yesterday
              unanimously passed a law that mandates punishment including a
              prison sentence of up to 20 years for anyone who instigates hate
              or violence on the radio, television or via social media.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Lethal Violence or Attack</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action of aggression by a government entity, organized group
              or individual that results in the death of one or more people,
              excluding crimes of passion.
              <br>
              <br>
              <b>Example:</b> “The Kaduna state government, Friday evening,
              disclosed that 33 women and children were killed by rebels in
              Kajuru local government of Kaduna state, less than twenty-four
              hours to the conduct of the presidential and parliamentary
              elections.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Martial Law / limits on gatherings</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The executive branch declares a state of emergency or suspends,
              temporarily or indefinitely, the ability of citizens to gather or
              protest against the order.
              <br>
              <br>
              <b>Example:</b> "On Wednesday, President Duterte approved the
              extension of martial law in the country's volatile south by a year
              due to continuing threats by Islamic State group-linked militants
              and communist insurgents.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Mobilize Security Forces</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>An event in which the government mobilizes police forces,
              military troops or government-affiliated militias.
              <br>
              <br>
              <b>Example:</b> “More than 500 security personnel have being
              mobilised for Saturday’s governorship election in Sokoto State,
              says the state commissioner of police (CP), Alhaji Adisa Bolanta.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Non - Lethal Violence</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action of aggression by a government entity, organized group
              or individual that physically harms one or more people or property
              but does NOT result in death, excluding crimes of passion.
              <br>
              <br>
              <b>Example:</b> “At least four persons were injured at Oruk Anam
              Local Government Area of Akwa Ibom State on during yesterday’s
              national election. Reports claim that unknown assailants attempted
              to snatch ballot boxes while voting was still ongoing.”
            </p>
          </section>

          <!-- <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h3><b>Praise</b></h3>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p>Defined as verbal expressions of admiration or approval of actors such as elites (political, economic, social), political groups or minorities. Examples include extolling the virtues of key political figures, applauding the political positions of policy makers, expressing admiration for certain business leaders, etc. This is a speech only category.
                                <br>
                                <br>
                                <b>Example:</b> “During his visit to Chennai, PM Narendra Modi praised the Tamil language and invoked the Tamil pride in his speeches. It is being seen as an attempt to assuage anti-Hindi sentiment prevalent across the state.”</p>
							</section> -->

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Protest</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Planned or spontaneous public mobilization of a large group of
              people. Labor strikes, political rallies and riots are also
              included in this category. When protest events involve deaths,
              they are coded as lethal violence, but this does not apply for
              non-lethal violence incidents.
              <br>
              <br>
              <b>Example:</b> “A reported two thousand people took to the
              streets yesterday in Nairobi to protest rising fuel prices, which
              have doubled since the beginning of the year.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Purge</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Purging refers to the removal or resignations of individuals from
              a government position. This includes the resignation of the chief
              executive. This description applies to purging of a target such as
              the bureaucracy, courts, military, police, state-owned companies,
              or members of political parties, among others.
              <br>
              <br>
              <b>Example:</b> “Poland’s government carried out a sweeping purge
              of the Supreme Court on Tuesday night, eroding the judiciary’s
              independence, escalating a confrontation with the European Union
              over the rule of law and further dividing this nation.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Raid</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Individuals or organizations are assaulted or aggressively
              coerced. Their property may be encroached or damaged as a result.
              Examples include a raid on newspaper offices. Victims themselves
              suffer no physical harm. This category also includes the
              government shutting down opposition organizations, NGOs, etc.
              <br>
              <br>
              <b>Example:</b> "A Vanguard newspaper office located on Bassey
              Duke Street was, yesterday afternoon, raided by hoodlums, who
              carted away large sums of money and destroyed computers and other
              equipment.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Threats</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A statement of a clear and explicit intention to inflict pain,
              injury, damage, or other hostile action on an individual or
              organization. Our targets and actors of interest are part of an
              opposition movement, the media, a political party, government, or
              a civil society organization.
              <br>
              <br>
              <b>Example 1:</b> “A top Kenyan newspaper published a fake death
              notice of a prominent opposition financier on Wednesday, a bizarre
              error that rights groups interpreted as another sign of an
              anti-democratic slide. The Daily Nation apologized by mid-morning
              for publishing the funeral announcement for businessman Jimi
              Wanjigi, whose picture, history and family details were used but
              whose name was slightly altered. The paper said the ad was
              published in error and it was working with police to uncover who
              placed it. After a week of arrests of opposition politicians and a
              crackdown on independent media, a prominent rights campaigner said
              the announcement amounted to a death threat to Wanjigi, who funded
              opposition leader Raila Odinga’s election campaign last year and
              whose house was raided by police in October.”
              <br>
              <br>
              <b>Example 2:</b> “Nigeria’s main labor unions threatened a
              nationwide strike over recent increases in gas prices.”
            </p>
          </section>


        </div>

        <div id="promo-wrapper">
          <section id="promo">
            <h2>Check out our forecasts</h2>
            <a href="forecasts.php" class="button">Forecasts</a>
          </section>
        </div>

      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>


</body>

</html>
