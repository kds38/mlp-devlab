<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title>Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2><b>Dimensions of Resurgent Authoritarian Influence</b></h2>
            <p id="RAICaption">The events below comprise the key components of
              Resurgent Authoritarian Influence we are tracking. We define an
              event as an action by an authoritarian government to wield
              influence within a developing economy. An event is extracted from
              a news article and is coded using machine learning to one of the
              22 types listed here. These event codings allow us to track RAI at
              high frequency, which in turn allows us to understand how these
              influence events affect civic space within a developing economy.
              This in turn gives us the ability to forecast how civic space
              might change in response to RAI events in coming months.</p>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Bribery/ Economic Corruption</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in bribery
              or corruption.
              <br>
              <br>
              <b>Example:</b> “There is a new twist in the multi-billion-dollar
              financial scandal surrounding the Malaysian investment fund 1MDB -
              possible Chinese involvement. Malaysia's new government - which
              took office only in May - has suspended three major construction
              projects with Chinese firms. A senior ministry official told the
              BBC that it believes that two of the contracts, for pipelines,
              were used to launder money for Malaysia's previous administration,
              led by the former Prime Minister, Najib Razak.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Cyber Operation</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in cyber
              intrusion or intelligence activities.
              <br>
              <br>
              <b>Example:</b> “Colombian authorities are investigating a series
              of cyber intrusions into voter registration systems that were
              discovered prior to the March parliamentary elections. One of the
              hacks was traced to a Venezuelan account, which some Colombian
              experts see as a Russian effort to extend influence through its
              relationships in Venezuela.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diaspora Activation</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor mobilizes citizens
              in a foreign nation to engage in domestic politics.
              <br>
              <br>
              <b>Example:</b> “A Chinese-born businessman, Jian was elected out
              of nowhere in 2015 to the Buenos Aires city legislature under the
              PRO party list, becoming the first PRC citizen to do so. The local
              press linked his nomination to the donation of US$1.2 million by
              Chinese businessmen to Macri’s party.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diplomatic Engagement</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in
              mediation between a local actor and others.
              <br>
              <br>
              <b>Example:</b> “Russia said on Tuesday it was ready to facilitate
              the start of dialogue between Venezuela's government and
              opposition but warned the United States against intervening in
              Caracas' internal affairs. Russia has sided with President Nicolas
              Maduro in his stand-off with opposition leader Juan Guaido.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diplomatic Relations</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor provides
              legitimacy through agreements or support.
              <br>
              <br>
              <b>Example:</b> “The comprehensive strategic partnership between
              Argentina and China was signed on July 18, 2014.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diplomatic Action</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in
              diplomatic sanctions.
              <br>
              <br>
              <b>Example:</b> “Russia expelled 23 British diplomats on Saturday
              in a carefully calibrated retaliatory move against London, which
              has accused the Kremlin of orchestrating a nerve toxin attack on a
              former Russian double agent and his daughter in southern England.
              Escalating a crisis in relations, Russia said it was also shutting
              down the activities of the British Council, which fosters cultural
              links between the two countries, and Britain's consulate-general
              in St. Petersburg.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diplomatic Statement</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in public
              discourse regarding local actors.
              <br>
              <br>
              <b>Example:</b> “The Maldives is a case in point, where Beijing
              supported President Abdulla Yameen after his declaration of a
              state of emergency and jailing of judges and opposition
              politicians.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Diplomatic Visit</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor visits with local
              officials.
              <br>
              <br>
              <b>Example:</b> “The 3rd Meeting of the Brazil-China Global
              Strategic Dialogue was held in Brasilia on July 25, co-chaired by
              the Minister of State for Foreign Affairs, Ambassador Ernesto
              Arajo, and by the State Councilor and Foreign Affairs Minister of
              China, Wang Yi.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Economic Aid</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor provides economic
              support.
              <br>
              <br>
              <b>Example:</b> “Recognizing Mongolia’s difficult situation, China
              Exim Bank agreed in early 2017 to provide financing under its $1
              billion line of credit at concessional rates for a hydropower
              project and a highway project from the airport to the capital.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Intelligence</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in
              intelligence or covert operations.
              <br>
              <br>
              <b>Example:</b> “Ukraine's security service SBU said on Wednesday
              it had captured a Russian military intelligence hit squad
              responsible for the attempted murder of a Ukrainian military spy
              in the run-up to a presidential election on Sunday.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Investment Action</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in
              investment in local projects.
              <br>
              <br>
              <b>Example:</b> “Most significantly, a $3 billion portion of the
              Central Asia-China gas pipeline (Line D) will pass through
              Tajikistan, reportedly financed through Chinese foreign direct
              investment (FDI), although there could be pressure for the Tajik
              government to cover some of the financing costs.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Security Force Exercise</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in joint
              security exercises or training.
              <br>
              <br>
              <b>Example:</b> “Thailand regularly conducts counter-terrorism
              exercises with China and launched its first ever joint air force
              training exercise with China in 2016.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Media Campaign/Intervention</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor interacts with
              local media.
              <br>
              <br>
              <b>Example:</b> “Grupo America, Argentina’s second-largest media
              corporation, closed an agreement with China Daily to insert the
              four-page China Watch supplement twice a month in five of the
              group’s newspapers, including El Cronista, the country’s top
              business daily.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Political Process and Policy Intervention</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor intervenes in
              local political processes.
              <br>
              <br>
              <b>Example:</b> “China has reportedly also donated funding to
              preferred political campaigns in Sierra Leone, the Philippines,
              and Sri Lanka.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Security Aid</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor provides security
              technology or supports military forces.
              <br>
              <br>
              <b>Example:</b> “Bolivia is in discussions with Moscow to purchase
              transport helicopters and YAK 130 interceptors as part of
              Bolivia’s military recapitalization program.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Security Engagement</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor enters into a
              security agreement with local actors.
              <br>
              <br>
              <b>Example:</b> “Russia and Bolivia signed a defense cooperation
              agreement in August 2017.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Security Force Presence</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor deploys security
              forces.
              <br>
              <br>
              <b>Example:</b> “China prepares to deploy nuclear submarines at
              Pakistan’s Gwadar Port.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Social/ Academic/ Cultural Exchange Technology</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in student
              or social exchange programs.
              <br>
              <br>
              <b>Example:</b> “In March 2017, representatives of the
              Ibero-American Center and Rossotrudnichestvo toured several
              countries in the region, including Argentina, to promote Russia’s
              academic offerings among Latin American students. In a
              presentation at the University of Buenos Aires, they announced
              that Russia would allocate 47 scholarships to Argentine applicants
              in 2017—up from fewer than 20 per year—out of a total of 15,000
              scholarships granted to international students every year by the
              Russian state.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Technology Action</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor provides
              technology.
              <br>
              <br>
              <b>Example:</b> “Technology linked to Chinese
              companies—particularly Huawei, Hikvision, Dahua, and ZTE—supply AI
              surveillance technology in sixty-three countries, thirty-six of
              which have signed onto China’s Belt and Road Initiative (BRI).”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Trade Agreement</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor signs a trade
              agreement.
              <br>
              <br>
              <b>Example:</b> “November 30, 2017 - A CZG member travelled to the
              Philippines to conduct economic and trade exchanges with the
              Philippine Chinese Economic and Trade federation.”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Financial Action</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor implements trade
              sanctions.
              <br>
              <br>
              <b>Example:</b> “Government sources in Buenos Aires mentioned
              Moscow’s threat to halt imports of Argentine beef and to withdraw
              a multibillion-dollar loan for a major infrastructure project in
              Argentina [in response to the Argentinian government’s
              announcement to end the deal with the television network RT en
              Espanol to broadcast on Argentina’s public television platform].”
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h3><b>Transnational Organized Crime</b></h3>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Any action where a foreign authoritarian actor engages in
              organized crime.
              <br>
              <br>
              <b>Example:</b> “The Philippines said Friday it has detained
              hundreds of Chinese workers in a continuing crackdown against
              unlicensed online gaming businesses catering to mainland
              customers.”
            </p>
          </section>

        </div>

        <div id="promo-wrapper">
          <section id="promo">
            <h2>Check out our forecasts</h2>
            <a href="forecasts.php" class="button">Forecasts</a>
          </section>
        </div>



      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
