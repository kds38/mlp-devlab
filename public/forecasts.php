<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>
      </div>

      <!-- Hero -->
      <section id="hero" class="container">
        <header>
          <br>
          <br>
          <h2>The Forecasting Process</h2>
        </header>
      </section>

    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">

        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2>For a more in-depth explanation of the forecasting process,
              please check out our <a href="pipelinereport.php">Pipeline
                Report</a>.</h2>
          </header>

        </article>
        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b> <a href="forecastingprocess.php">The Forecasting
                    Process</a> </b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>For more information about key concepts underpinning the machine
              learning approach we use, please see this page.</p>
          </section>

          <!-- <section class="col-4 col-12-narrower aln-middle feat">
								<header>
                                    <h2><b> <a href= "forecast_terms.php">Forecast Key Terms</a> </b></h2>
								</header>
							</section>
							<section class="col-8 col-12-narrower aln-middle">
								<p> For more information about key definitions related to machine learning, please see this page.</p>
							</section> -->


          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b> <a href="shiny_app.php">Forecast</a> </b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p> For a visualization of our forecasts, please see this page.</p>
          </section>


        </div>


      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
