<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">DevLab@Duke</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>

      </div>
    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">


        <!-- Content -->
        <article id="content">
          <header class="major">
            <h2>Machine Learning Key Terms</h2>
            <p>Some commonly used key terms.</p>
            <a href="#" class="image featured"><img
                src="images/nasa-Q1p7bh3SHj8-unsplash.jpg" alt="" /></a>
          </header>

        </article>

        <div class="row features">

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Classification Model</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A machine learning model that categorizes values according to two
              or more discrete classes. For example, a natural language
              processing classification model could determine whether a news
              article is reporting on a protest, a change to electoral laws, or
              some other type of event.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Cross - Validation</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A procedure for estimating how well a model performs at making
              classifications or predictions. Cross-validation repeatedly tests
              a model on different subsets of a dataset to measure performance.
            </p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Features</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Attributes of an example, either numerical or categorical.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Lasso Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Lasso regression is a type of analysis used in machine learning
              that engages in both variable selection as well as regularization.
              The goal is to enhance the prediction accuracy and
              interpretability of the statistical model produced.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Layer</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A collection of nodes operating together at the same depth within
              a neural network.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Linear Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A regression that uses conditional means to analyze how a dataset
              predicts a target variable.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Logistic Regression</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A regression algorithm that produces a model that generates a
              probability for each discrete label value in classification
              problems using a sigmoid function.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Machine Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A sub-field of artificial intelligence. Machines learn to
              identify patterns in data, build models that explain the world,
              and make predictions from datasets.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Natural Language Processing (NLP)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Systems through which computers learn to process large amounts of
              human spoken language.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Neural Networks</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A model designed to connect multiple nodes in layers to analyze a
              dataset.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Node</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A ‘neuron’ within a neural network. It receives one or more
              weighted inputs and sends an output to the nodes in the next layer
              determined by an activation function. Nodes are organized into
              layers to comprise a network.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Overfitting</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Overfitting occurs when the model predicts the training set so
              accurately that it can’t produce reliable predictions on new data.
              This happens when the model incorporates noise or details that are
              specific to the training set. When accuracy over the training data
              increases and accuracy in the validation data stays the same or
              decreases, we are overfitting.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Principal Component Analysis (PCA)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A statistical procedure that uses an orthogonal linear
              transformation of data which projects n examples with i features
              onto a hyper plane to minimize projection error. This process
              converts a set of observations of potentially correlated variables
              into a set of linearly uncorrelated variables called principal
              components.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Reinforcement Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A subfield of machine learning where the machine perceives its
              environment’s state as a vector of features. The machine can
              execute actions which provide different rewards. The goal of
              reinforcement learning is for the machine to learn a policy, which
              is the optimal action to perform in each state.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Supervised Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The problem of learning a model for which labeled examples are
              available.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Support Vector Machines (SVMs)</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Models used for classification that seeks to maximize the
              distance between positive and negative classes.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Target Variable</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>The quantity that the model tries to predict.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Test Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>Data that is held out from the dataset and is used to assess
              final model validity.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Training Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A collection of examples from the dataset that the algorithm uses
              to create a model.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Unsupervised Learning</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A problem where, given an unlabeled dataset, the algorithm seeks
              to find hidden (or latent) structure.</p>
          </section>

          <section class="col-4 col-12-narrower aln-middle feat">
            <header>
              <h2><b>Validation Set</b></h2>
            </header>
          </section>
          <section class="col-8 col-12-narrower aln-middle">
            <p>A holdout dataset used to conduct testing for hyperparameter
              tuning with the goal of minimizing overfitting.</p>
          </section>

        </div>
      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>

</body>

</html>
