<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>>

      </div>

      <!-- Hero -->
      <section id="hero" class="container">
        <header>
          <br>
          <br>
          <h2 id="Our Team"> Our Team
          </h2>
        </header>
      </section>
    </div>



    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">

        <div class="row features">

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Adiguzel_image.png" alt="" /></a>
            </div>
            <header>
              <h3><a href="https://serkantadiguzel.com/">Serkant Adiguzel</a>
              </h3>
            </header>
            <p id="Serkant">Serkant Adiguzel is a Ph.D. student in political
              science at Duke. He is specializing in Political Economy with a
              particular interest in redistribution and regime transitions.He
              helps oversee validation and training of the machine models for
              the Peace-Project.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img src="images/Andreas.jpeg"
                  alt="" /></a>
            </div>
            <header>
              <h3><a href="https://www.andybeger.com/">Andreas Beger</a></h3>
            </header>
            <p id="Andreas">Andreas Beger is an independent consultant with 10
              years experience in developing data-driven geopolitical
              forecasting systems. He has worked on projects funded by the
              Intelligence Advanced Research Projects Activity (IARPA), the
              Political Instability Task Force (PITF), the Varieties of
              Democracy (V-Dem) Institute, and others. He received a Ph.D. in
              political science from Florida State University in 2012.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img src="images/Mateo_IMG.png"
                  alt="" /></a>
            </div>
            <header>
              <h3><a href="https://mateovillamizarchaparro.github.io/">Mateo
                  Villamizar Chaparro</a></h3>
            </header>
            <p id="Mateo">Mateo is a PhD student in political economy at the
              department of Political Science. His research interests include
              analyzing the politics of public goods' distribution and violence
              in developing countries. With an emphasis in economic development,
              migration, state capacity and political institutions.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img src="images/Lin_IMG.jpg"
                  alt="" /></a>
            </div>
            <header>
              <h3>Zung-Ru Lin</h3>
            </header>
            <p id="Zung-Ru">Zung-Ru is working as a Data Scientist that helps
              solve technical issues encountered. His work mainly focuses on
              optimizing the overall functionality and productivity within the
              peace project pipeline framework and implementing NLP and various
              supervised machine learning methods in detecting civic-space
              changes.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Moratz_image.png" alt="" /></a>
            </div>
            <header>
              <h3>Donald Moratz</h3>
            </header>
            <p id="Donald">Donald Moratz is a Research Manager at the
              DevLab@Duke and the Department of Political Science at Duke
              University. His work focuses on the Machine Learning for Peace
              project and the integration of advanced numerical methods in
              political science. His substantive areas of interest are in the
              political economy of development as well as endogenous growth.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Romero_image.png" alt="" /></a>
            </div>
            <header>
              <h3><a href="https://diego-romero.com/">Diego Romero</a></h3>
            </header>
            <p id="Diego">Diego is a PhD candidate at Duke University’s
              Political Science Department specializing in Political Economy and
              Political Methodology. He is interested in issues of governance,
              in particular corruption and accountability. His other research
              interests include reintegration of deported migrants and the
              spatial distribution of public service delivery.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Springman_image.png" alt="" /></a>
            </div>
            <header>
              <h3><a href="https://sites.duke.edu/jeremyspringman/">Jeremy
                  Springman</a></h3>
            </header>
            <p id="Jeremy">Jeremy is a Postdoctoral Research Associate at the
              DevLab@Duke and the Department of Political Science. His research
              uses spatial data and field experiments to study the political
              economy of development and the influence of non-profit
              organizations on politics and governance in Africa. His work has
              received support from the National Science Foundation and an
              anonymous private foundation, among others.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Hanling_IMG.png" alt="" /></a>
            </div>
            <header>
              <h3><a href="https://www.linkedin.com/in/hanling-su/">Hanling
                  Su</a></h3>
            </header>
            <p id="Hanling">Hanling joined Devlab@Duke as a Data Scientist. She
              works mainly on data engineering that focuses on web scraping and
              data preparation. By applying machine learning and natural
              language processing techniques, Hanling ensures the project’s data
              integrity. She is also responsible for implementing the data
              pipeline for Machine Learning for Peace project at DevLab.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured center"><img
                  src="images/Wibbels_image.png" alt="" /></a>
            </div>
            <header>
              <h3><a href="https://sites.duke.edu/wibbels/">Erik Wibbels</a>
              </h3>
            </header>
            <p id="Erik">Dr. Wibbels' research focuses on development,
              decentralized governance, and other areas of political economy. He
              has worked with USAID’s DRG Centre, USAID mission officers,
              AidData, RTI International, the World Bank and others on projects
              around the world. Erik is the PI on the Machine Learning for Peace
              project. </p>
          </section>

        </div>

        <header>
          <h2 id="Intern_Header">MELD Interns</h2>
        </header>

        <div class="row features">

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Sanjit_IMG.png" alt="" /></a>
            </div>
            <header>
              <h3>Sanjit Beriwal</h3>
            </header>
            <p id="Sanjit">Sanjit Beriwal is an undergraduate student at Duke
              studying economics and political science. He currently works as a
              MELD intern for DevLab@Duke. He assists in governance research
              through the INSPIRES program and democratic backsliding research
              funded by USAID.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img
                  src="images/Jonathan_IMG.jpg" alt="" /></a>
            </div>
            <header>
              <h3>Jonathan Sandoval</h3>
            </header>
            <p id="Jonathan">Jonathan Sandoval is an undergraduate at Duke
              studying Electrical & Computer Engineering and Computer Science.
              He currently works as a MELD intern for DevLab@Duke. He assists in
              web scraping for data, while also maintaining our website.</p>

          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img src="images/Mike_IMG.jpg"
                  alt="" /></a>
            </div>
            <header>
              <h3>Mike Sun</h3>
            </header>
            <p id="Mike">Mike Sun is a masters student at Duke studying
              political economy. His research interests are behavioral politics
              and behavioral studies on people’s reactions to information/policy
              changes.</p>
          </section>

          <section class="col-4 col-12-narrower feature">
            <div class="image-wrapper">
              <a href="#" class="image featured"><img src="images/Huong_IMG.png"
                  alt="" /></a>
            </div>
            <header>
              <h3>Huong Vu</h3>
            </header>
            <p id="Huong">Huong Vu is a Master student in International
              Development Policy at Sanford School of Public Policy, Duke
              University. She is interested in international development and
              project evaluation. Her work focuses on poverty reduction, social
              safety nets and disaster risk management.</p>
          </section>


        </div>

        <header class="major">
          <h2 id="Thanks">We thank Clara Suong and Joan Timoneda for earlier
            work on MLP. This project would not have been possible without the
            key contributions of Scott de Marchi and Spencer Dorsey.</h2>
        </header>

      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
