<!DOCTYPE HTML>
<!--
	Telephasic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
  <title id="title">Telephasic by HTML5 UP</title>
  <meta charset="utf-8" />
  <meta name="viewport"
    content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="//mlp.trinity.duke.edu/assets/css/main.css" />
</head>

<body class="no-sidebar is-preload">
  <div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
      <div id="header" class="container">

        <!-- Logo -->
        <h1 id="logo"><a href="index.php">ML for Peace</a></h1>

        <!-- Nav -->
        <nav id="nav">
          <ul>
            <li><a href="#" id="Civic_Space_Data">Civic Space Data</a>
              <ul>
                <li><a href="shiny_app.php" id="Forecasts_Link">Forecasts</a>
                </li>
                <li><a href="kibana.php" id="Data_Dashboard_Link">Data
                    Dashboard</a></li>
              </ul>
            </li>
            <li><a href="#">Ukraine Data</a>
              <ul>
                <li><a href="dataforukraine.php">#DataforUkraine</a></li>
                <li><a href="mapofukraine.php">#MapofUkraine</a></li>
                <li><a href="disinformation.php">#Disinformation</a></li>
                <li><a href="who_we_are.php">Who We Are</a></li>
              </ul>
            </li>
            <li class="break"></li>
            <li><a href="technical_details.php"
                id="Technical_Details_&_Research_Link">Technical Details</a>
            </li>
            <li><a href="team.php" id="Our_Team Link">Our Team</a></li>
          </ul>
        </nav>
      </div>

      <!-- Hero -->
      <section id="hero" class="container">
        <header>
          <br>
          <br>
          <h2 id="Data Dashboard"> Data Dashboard</h2>
        </header>
      </section>


    </div>

    <!-- Main -->
    <div class="wrapper">
      <div class="container" id="main">

        <!-- Content -->
        <article id="content">
          <header class="major">

          </header>

          <iframe style="width: 100%"
            src="https://mlp-devlab.shinyapps.io/descriptive/"
            height="1900"></iframe>

          <!-- <header class = "major">
									<h3 id="Visualization">Visualization of how articles are coded can be seen below</h3>
									<br>
									<h5><ul>
										<li id="first bullet">Utilize the filters located above the map to filter the data on <b>RAI Event Type</b> and <b>Civic Space Event</b></li>
										<li id="second bullet">Utilize the time filter by clicking the three dots in the top right to view events within different <b>time periods within 2020</b></li>
									</ul></h5>
								</header>
								<iframe src="https://ml4p.kb.eastus2.azure.elastic-cloud.com:9243/app/dashboards#/view/68f3f430-1a8a-11ec-b0da-fd8f56270580?embed=true&_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3A'2018-09-21T02%3A55%3A40.557Z'%2Cto%3Anow))&show-query-input=true&show-time-filter=true" height="920" width="1200"></iframe> -->
          <!-- <iframe src="https://i-o-optimized-deployment-45547b.kb.eastus2.azure.elastic-cloud.com:9243/app/dashboards?auth_provider_hint=anonymous1#/view/b9052620-d8fd-11eb-968e-59a335ecbbf4?embed=true&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(description:'',filters:!(),fullScreenMode:!f,options:(hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(attributes:(description:'',layerListJSON:'%5B%7B%22sourceDescriptor%22:%7B%22type%22:%22EMS_TMS%22,%22isAutoSelect%22:true%7D,%22id%22:%221f0fdb80-2649-41fb-89f1-3f6c03da421e%22,%22label%22:null,%22minZoom%22:0,%22maxZoom%22:24,%22alpha%22:1,%22visible%22:true,%22style%22:%7B%22type%22:%22TILE%22%7D,%22type%22:%22VECTOR_TILE%22%7D,%7B%22sourceDescriptor%22:%7B%22indexPatternId%22:%222e7ee0b0-d8fb-11eb-968e-59a335ecbbf4%22,%22geoField%22:%22location%22,%22filterByMapBounds%22:true,%22scalingType%22:%22CLUSTERS%22,%22id%22:%22bae9f3d0-0c93-4560-8c87-50017d87955a%22,%22type%22:%22ES_SEARCH%22,%22applyGlobalQuery%22:true,%22applyGlobalTime%22:true,%22tooltipProperties%22:%5B%5D,%22sortField%22:%22%22,%22sortOrder%22:%22desc%22,%22topHitsSplitField%22:%22%22,%22topHitsSize%22:1%7D,%22id%22:%2251b956b9-9a82-4c41-9e2c-5b7d8717c97c%22,%22label%22:%22Events%22,%22minZoom%22:0,%22maxZoom%22:24,%22alpha%22:0.75,%22visible%22:true,%22style%22:%7B%22type%22:%22VECTOR%22,%22properties%22:%7B%22icon%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22value%22:%22marker%22%7D%7D,%22fillColor%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22color%22:%22%2354B399%22%7D%7D,%22lineColor%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22color%22:%22%2341937c%22%7D%7D,%22lineWidth%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22size%22:1%7D%7D,%22iconSize%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22size%22:6%7D%7D,%22iconOrientation%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22orientation%22:0%7D%7D,%22labelText%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22value%22:%22%22%7D%7D,%22labelColor%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22color%22:%22%23000000%22%7D%7D,%22labelSize%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22size%22:14%7D%7D,%22labelBorderColor%22:%7B%22type%22:%22STATIC%22,%22options%22:%7B%22color%22:%22%23FFFFFF%22%7D%7D,%22symbolizeAs%22:%7B%22options%22:%7B%22value%22:%22circle%22%7D%7D,%22labelBorderSize%22:%7B%22options%22:%7B%22size%22:%22SMALL%22%7D%7D%7D,%22isTimeAware%22:true%7D,%22type%22:%22BLENDED_VECTOR%22,%22joins%22:%5B%5D%7D%5D',mapStateJSON:'%7B%22zoom%22:2.13,%22center%22:%7B%22lon%22:-42.28013,%22lat%22:19.8017%7D,%22timeFilters%22:%7B%22from%22:%222006-06-28T16:27:48.941Z%22,%22to%22:%22now%22%7D,%22refreshConfig%22:%7B%22isPaused%22:false,%22interval%22:10000%7D,%22query%22:%7B%22query%22:%22%22,%22language%22:%22kuery%22%7D,%22filters%22:%5B%5D,%22settings%22:%7B%22autoFitToDataBounds%22:false,%22backgroundColor%22:%22%23ffffff%22,%22disableInteractive%22:false,%22disableTooltipControl%22:false,%22hideToolbarOverlay%22:false,%22hideLayerControl%22:false,%22hideViewControl%22:false,%22initialLocation%22:%22LAST_SAVED_LOCATION%22,%22fixedLocation%22:%7B%22lat%22:0,%22lon%22:0,%22zoom%22:2%7D,%22browserLocation%22:%7B%22zoom%22:2%7D,%22maxZoom%22:24,%22minZoom%22:0,%22showScaleControl%22:false,%22showSpatialFilters%22:true,%22spatialFiltersAlpa%22:0.3,%22spatialFiltersFillColor%22:%22%23DA8B45%22,%22spatialFiltersLineColor%22:%22%23DA8B45%22%7D%7D',title:Locations,uiStateJSON:'%7B%22isLayerTOCOpen%22:true,%22openTOCDetails%22:%5B%5D%7D'),enhancements:(),hiddenLayers:!(),isLayerTOCOpen:!t,mapBuffer:(maxLat:26.068275,maxLon:-63.92556999999999,minLat:14.424335,minLon:-85.86541),mapCenter:(lat:20.27361,lon:-74.89549,zoom:5.45),openTOCDetails:!()),gridData:(h:15,i:f5ded684-ab87-461c-9063-88c66723c234,w:24,x:0,y:0),panelIndex:f5ded684-ab87-461c-9063-88c66723c234,type:map,version:'7.13.2')),query:(language:kuery,query:''),tags:!(),timeRestore:!f,title:'Test%20Dashboard',viewMode:view)" height="920" width="1200"></iframe> -->
        </article>
      </div>
    </div>

    <!-- Footer -->
    <div id="footer-wrapper">

      <div id="copyright" class="container">
        <ul class="menu">
          <li id="lang_en"> <a href="#en"> English </a>
          <li id="lang_es"> <a href="#es"> Spanish </a> <br>
        </ul>
        <ul class="menu">
          <li>&copy; Untitled. All rights reserved.</li>
          <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dropotron.min.js"></script>
  <script src="assets/js/browser.min.js"></script>
  <script src="assets/js/breakpoints.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/getText.js"></script>

</body>

</html>
