function changeText(language){
  let request = new XMLHttpRequest();
  var requestoorl = "languages/" + language + ".json"

  request.open('GET', requestoorl);
  request.responseType = 'json';
  request.send();
  request.onload = function() {
    window.location.hash = language;
    const lang = request.response;
    // location.reload();
    for (var key in lang){
      var divText = document.getElementById(key);
      console.log(key)
      if (divText == null){
        continue;
      } else {
        divText.innerHTML = JSON.parse(JSON.stringify(lang[key]));
      }
    }
  }

}

if (window.location.hash){
  changeText(window.location.hash.substr(1, window.location.hash.length));
} else {
  window.location.hash = 'en'
}

window.onhashchange = function(e){
  // e.preventDefault();
  changeText(window.location.hash.substr(1, window.location.hash.length));
  location.reload();
}


